# firemaw-fr

After seeing too much "feelscrafting" and unsound application of maths / probability theory on the topic of fire resistance and "Flame Buffet" (Ability used by WoW boss [Firemaw](https://classic.wowhead.com/npc=11983/firemaw) :-D). I decided to solve the problem properly by modeling the encounter as a markov chain using [stochastic matrices](https://en.wikipedia.org/wiki/Stochastic_matrix).

It was a nice opportunity to use some of the skills learnt in school for an actual "real world" problem. The analysis + a few probability distribution plots are included in **firemaw_fr.ipynb**.
